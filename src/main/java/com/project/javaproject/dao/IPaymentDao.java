package com.project.javaproject.dao;

import com.project.javaproject.entities.Payment;

import java.util.List;

public interface IPaymentDao {
    public int rowcount();
    public List<Payment> findAll();
    public List<Payment> findByCustId(String type);
    public void save(Payment payment);
}
