package com.project.javaproject.dao;

import com.project.javaproject.entities.Payment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class PaymentDao implements IPaymentDao {

    @Autowired
    private MongoTemplate mongoTemplate;

    @Override
    public int rowcount() {
        return mongoTemplate.findAll(Payment.class).size();
    }

    @Override
    public List<Payment> findAll() {
        return mongoTemplate.findAll(Payment.class);
    }

    @Override
    public List<Payment> findByCustId(String type) {
        Query query = new Query();
        query.addCriteria(Criteria.where("custid").is(type));
        List<Payment> payment = mongoTemplate.find(query, Payment.class);
        return payment;
    }

    @Override
    public void save(Payment payment) {
        mongoTemplate.insert(payment);
    }
}
