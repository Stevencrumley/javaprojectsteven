package com.project.javaproject.entities;

import java.util.Date;

public class Payment {
    private int id;
    private Date paymentdate;
    private double amount;
    private int custid;

    public Payment(int id, Date paymentdate, double amount, int custid) {
        this.id = id;
        this.paymentdate = paymentdate;
        this.amount = amount;
        this.custid = custid;
    }

    public Payment() {
    }

    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        return stringBuilder.append(id).append(paymentdate).append(amount).append(custid).toString();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getPaymentdate() {
        return paymentdate;
    }

    public void setPaymentdate(Date paymentdate) {
        this.paymentdate = paymentdate;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public int getCustid() {
        return custid;
    }

    public void setCustid(int custid) {
        this.custid = custid;
    }


}
