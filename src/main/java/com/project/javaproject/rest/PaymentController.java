package com.project.javaproject.rest;

import com.project.javaproject.entities.Payment;
import com.project.javaproject.services.IPaymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api")
public class PaymentController {

    @Autowired
    IPaymentService service;

    @RequestMapping(value = "/status", method = RequestMethod.GET)
    public String status()
    {
        return "Service is working";
    }

    @RequestMapping(value = "/rowcount", method = RequestMethod.GET)
    public int rowCount()
    {
        return service.rowCount();
    }

    @RequestMapping(value="findall", method=RequestMethod.GET)
    public List<Payment> findAll()
    {
        return service.findAll();
    }

    @RequestMapping(value="findbycustid/{id}", method=RequestMethod.GET)
    public List<Payment> findById(@PathVariable("id") String custId)
    {
        return service.findByCustId(custId);
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public void save(@RequestBody Payment payment)
    {
        service.save(payment);
    }
}
