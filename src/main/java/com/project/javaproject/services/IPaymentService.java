package com.project.javaproject.services;

import com.project.javaproject.entities.Payment;

import java.util.List;

public interface IPaymentService {
    public int rowCount();
    public List<Payment> findAll();
    public List<Payment> findByCustId(String type);
    public void save(Payment payment);
}
