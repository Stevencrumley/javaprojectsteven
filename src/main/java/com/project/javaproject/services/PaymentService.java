package com.project.javaproject.services;

import com.project.javaproject.dao.PaymentDao;
import com.project.javaproject.entities.Payment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PaymentService implements IPaymentService {
    @Autowired
    private PaymentDao dao;

    @Override
    public int rowCount() {
        return dao.rowcount();
    }

    @Override
    public List<Payment> findAll() {
        return dao.findAll();
    }

    @Override
    public List<Payment> findByCustId(String type)
    {
        if(type != null) {
            return dao.findByCustId(type);
        }
        else
        {
            return null;
        }
    }

    @Override
    public void save(Payment payment) {
        dao.save(payment);
    }
}
