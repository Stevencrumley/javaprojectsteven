package com.project.javaproject.dao;

import com.project.javaproject.entities.Payment;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.util.AssertionErrors.assertNotNull;

@SpringBootTest
public class PaymentDaoTest {
    @Autowired
    private IPaymentDao dao;

    @Test
    public void testRowCount()
    {
        assertEquals(dao.rowcount(), 1);
    }

    @Test
    public void testFindById()
    {
        Payment payment = dao.findById(1);
        assertNotNull("Check if payment is found by Id", payment);
    }

    @Test
    public void testFindByCustId()
    {
        List<Payment> payment = dao.findByCustId("123");
        assertNotNull("Check if payment is found", payment);
    }

    @Test
    public void testSave()
    {
        Payment payment = new Payment(1,new Date(2020,7,21),100,123);
        dao.save(payment);
        payment = dao.findById(1);
        assertNotNull("Check if payment is found", payment);
    }
}
