package com.project.javaproject.entities;


import org.junit.jupiter.api.Test;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PaymentTest {

    @Test
    public void testPaymentToString()
    {
        Payment payment = new Payment(1,new Date(2020,9,15),100,123);
        assertEquals("1Fri Oct 15 00:00:00 UTC 3920100.0123", payment.toString());
    }
}
