package com.project.javaproject.services;

import com.project.javaproject.entities.Payment;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.util.AssertionErrors.assertNotNull;

@SpringBootTest
public class PaymentServiceTest {
    @Autowired
    IPaymentService paymentService;

    @Test
    public void testRowCount()
    {
        assertEquals(paymentService.rowCount(), 1);
    }

    @Test
    public void testFindById()
    {
        Payment payment = paymentService.findById(1);
        assertNotNull("Check if payment is found by Id", payment);
    }

    @Test
    public void testFindByCustId()
    {
        List<Payment> payment = paymentService.findByCustId("123");
        assertNotNull("Check if payment is found", payment);
    }

    @Test
    public void testSave()
    {
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        Date date = new Date();
        Payment payment = new Payment(1,date,100,123);
        paymentService.save(payment);
        payment = paymentService.findById(1);
        assertNotNull("Check if payment is found", payment);
    }
}
